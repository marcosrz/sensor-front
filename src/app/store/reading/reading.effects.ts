import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import * as fromRoot from '../../store' 
import * as ReadingActions from './reading.actions';
import { SensorService } from 'src/app/services/sensor.service';
import { Store, select } from '@ngrx/store';



@Injectable()
export class ReadingEffects {

  loadReadings$ = createEffect(() => this.actions$.pipe( 

      ofType(ReadingActions.loadReadings),
      withLatestFrom(this.store$.pipe(select(fromRoot.selectReadingState))),
      switchMap(([action, readingState]) => {
        
        const { from, to } = readingState;

        // if (to < from) {
        //   return of(ReadingActions.loadReadingsFailure({ error: { message: '"To" date sould be greater than "From"', to, from } }));
        // }

        // const from = new Date(new Date().setHours(0,0,0,0));
        // const to = new Date(new Date().setHours(23,59,59,999));

        return this.sensorService.getReadings('x', from, to).pipe(
          map(data => ReadingActions.loadReadingsSuccess({ data })),
          catchError(error => of(ReadingActions.loadReadingsFailure({ error })))
        )
        
      })
  ));

  updateSingularValues$ = createEffect(() => this.actions$.pipe( 

      ofType(ReadingActions.loadReadingsSuccess),
      map((action) => {
      
        console.log('Updating singular values')

        const readings = action.data;

        const values = readings.map(reading => Number(reading.value));

        const avgValue = values.reduce((sum, i) => sum + i, 0) / action.data.length;
        const minValue = Math.min(...values);
        const maxValue = Math.max(...values);
      
        const min = readings.find(reading => reading.value === minValue);
        const max = readings.find(reading => reading.value === maxValue);
        const avg = { ...max, value: avgValue };
        const cur = readings[readings.length - 1];

        return ReadingActions.updateSingularValues({ data: { min, max, avg, cur } });

      })
  ));

  constructor(private store$: Store<fromRoot.State>, private actions$: Actions, private sensorService: SensorService) {}

}
