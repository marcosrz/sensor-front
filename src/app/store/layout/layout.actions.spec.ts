import * as fromLayout from './layout.actions';

describe('toggleSidebar', () => {
  it('should return an action', () => {
    expect(fromLayout.toggleSidebar().type).toBe('[Layout] Toggle Sidebar');
  });
});
