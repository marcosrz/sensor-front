import { Action, createReducer, on, createSelector } from '@ngrx/store';
import * as LayoutActions from './layout.actions';

export const layoutFeatureKey = 'layout';

export interface State {
  showSidebar: Boolean
}

export const initialState: State = {
  showSidebar: false
};

export const selectShowSidebar = (state: State) => state.showSidebar;

const layoutReducer = createReducer(
    initialState,

    on(LayoutActions.toggleSidebar, state => ({...state, showSidebar: !state.showSidebar })),
    on(LayoutActions.showSidebar, state => ({...state, showSidebar: true })),
    on(LayoutActions.hideSidebar, state => ({...state, showSidebar: false })),
);

export function reducer(state: State | undefined, action: Action) {
  return layoutReducer(state, action);
}

