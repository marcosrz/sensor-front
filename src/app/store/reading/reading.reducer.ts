import { Action, createReducer, on, createSelector, createFeatureSelector } from '@ngrx/store';
import * as ReadingActions from './reading.actions';
import { Reading } from 'src/app/models/Reading';

export const readingFeatureKey = 'reading';

export interface State {
  readings: Reading[],
  loading: Boolean,
  avg: Reading,
  min: Reading,
  max: Reading,
  cur: Reading,
  from: Date,
  to: Date,
}

export const initialState: State = {
  readings: [],
  loading: false,
  avg: null,
  min: null,
  max: null,
  cur: null,
  from: null,
  to: null,
};

export const selectReadings = (state: State) => state.readings;
export const selectAvg = (state: State) => state.avg;
export const selectMax = (state: State) => state.max;
export const selectMin = (state: State) => state.min;
export const selectCur = (state: State) => state.cur;
export const selectFrom = (state: State) => state.from;
export const selectTo = (state: State) => state.to;

const readingReducer = createReducer(
  initialState,

  on(ReadingActions.loadReadings, state => ({ ...state, loading: true })),
  on(ReadingActions.loadReadingsSuccess, (state, action) => ({ ...state, readings: action.data, loading: false })),
  on(ReadingActions.loadReadingsFailure, (state, action) => ({ ...state, loading: false })),
  on(ReadingActions.updateSingularValues, (state, action) => ({ ...state, ...action.data })),
  on(ReadingActions.setFrom, (state, action) => ({ ...state, from: new Date(new Date(action.data).setHours(0,0,0,0)).toUTCString() })),
  on(ReadingActions.setTo, (state, action) => ({ ...state, to: new Date(new Date(action.data).setHours(23,59,59,999)).toUTCString() })),

);

export function reducer(state: State | undefined, action: Action) {
  return readingReducer(state, action);
}