import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { ROUTER_NAVIGATION } from '@ngrx/router-store'
import { toggleSidebar, hideSidebar } from '../layout/layout.actions';
import { mapTo } from 'rxjs/operators';

@Injectable()
export class RouterEffects {

  routerNavigation$ = createEffect(() =>  this.actions$.pipe( 
      ofType(ROUTER_NAVIGATION),
      mapTo(hideSidebar())
  ));

  constructor(private actions$: Actions) {}

}
