import * as fromReading from './reading.reducer';
import { selectReadingState } from './reading.selectors';

describe('Reading Selectors', () => {
  it('should select the feature state', () => {
    const result = selectReadingState({
      [fromReading.readingFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
