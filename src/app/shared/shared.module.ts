import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentReadingComponent } from './current-reading/current-reading.component';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { ReadingsListComponent } from './readings-list/readings-list.component';
import { ReadingsChartComponent } from './readings-chart/readings-chart.component';
import { ChartsModule } from 'ng2-charts';
import { MatTabsModule } from '@angular/material/tabs'
import { MatIconModule, MatIcon } from '@angular/material/icon';
import {MatDatepickerModule, MatDatepicker} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  declarations: [CurrentReadingComponent, ReadingsListComponent, ReadingsChartComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatTabsModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    CommonModule,
    ChartsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports: [
    CurrentReadingComponent,
    ReadingsListComponent,
    ReadingsChartComponent,
    MatDatepickerModule,
    ChartsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIcon,
    MatButtonModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
