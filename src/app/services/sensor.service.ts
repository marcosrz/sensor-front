import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reading } from '../models/Reading';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/ApiResponse';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SensorService {

  private API_URL = '';
  private SENSOR_READINGS_URL = `/sensors/5ed4fff18559dc0c28943c28/readings`;

  constructor(private httpClient: HttpClient) { 

    this.API_URL = environment.API_URL_BASE;
  }

  getReadings(sensorId: String, from: Date, to:Date): Observable<Reading[]> {

    const url = `${this.API_URL}${this.SENSOR_READINGS_URL}?from=${from}&to=${to}`;

    return this.httpClient.get<ApiResponse<Reading[]>>(url).pipe(map(response => response.payload));
  }
}
