import { createAction, props } from '@ngrx/store';
import { Reading } from 'src/app/models/Reading';

export const loadReadings = createAction(
  '[Reading] Load Readings'
);

export const loadReadingsSuccess = createAction(
  '[Reading] Load Readings Success',
  props<{ data: Reading[] }>()
  );
  
export const loadReadingsFailure = createAction(
  '[Reading] Load Readings Failure',
  props<{ error: any }>()
);

export const updateSingularValues = createAction(
  '[Reading] Update Singular Values',
  props<{ data: any }>()
);

export const setFrom = createAction(
  '[Reading] Set from date',
  props<{ data: any }>()
);

export const setTo = createAction(
  '[Reading] Set to date',
  props<{ data: any }>()
);

