import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import {  MatListModule } from '@angular/material/list';
import {  MatButtonModule } from '@angular/material/button';
import { LayoutComponent } from './layout/layout.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import { HomeComponent } from '../pages/home/home.component';

@NgModule({
  declarations: [MenuComponent, LayoutComponent, SidebarComponent],
  imports: [
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'dashboard', component: HomeComponent}
    ]),
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
  ],
  exports: [LayoutComponent]
})
export class LayoutModule { }
