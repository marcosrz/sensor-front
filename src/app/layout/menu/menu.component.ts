import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as fromRoot from '../../store';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { toggleSidebar } from 'src/app/store/layout/layout.actions';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private store: Store<fromRoot.State>) { 
    
  }

  onToggleSidebarClick(){
    this.store.dispatch(toggleSidebar());
  }

  ngOnInit() {}

}
