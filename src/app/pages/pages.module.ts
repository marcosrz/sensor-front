import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { MatTableModule} from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from '../shared/shared.module';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [HomeComponent, AboutComponent],
  imports: [
    SharedModule,
    MatTableModule,
    MatTabsModule,
    MatCardModule,
    CommonModule
  ]
})
export class PagesModule { }
