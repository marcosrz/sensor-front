import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ReadingEffects } from './reading.effects';

describe('ReadingEffects', () => {
  let actions$: Observable<any>;
  let effects: ReadingEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ReadingEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(ReadingEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
