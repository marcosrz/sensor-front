import { Component, OnInit, Input } from '@angular/core';
import { Reading } from 'src/app/models/Reading';

@Component({
  selector: 'app-readings-list',
  templateUrl: './readings-list.component.html',
  styleUrls: ['./readings-list.component.scss']
})
export class ReadingsListComponent implements OnInit {

  public displayedColumns = ['date', 'time', 'value'];

  @Input() title: String;
  @Input() description: String;
  @Input() readings: Reading[] = [];

  constructor() { }

  ngOnInit() {
  }

}
