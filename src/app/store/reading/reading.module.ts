import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { ReadingEffects } from './reading.effects';
import { StoreModule } from '@ngrx/store';
import * as fromReading from './reading.reducer';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromReading.readingFeatureKey, fromReading.reducer),
  ]
})
export class ReadingModule { }
