import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../store';
import { Observable } from 'rxjs';
import { toggleSidebar } from 'src/app/store/layout/layout.actions';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public showSidebar$: Observable<Boolean>;

  constructor(private store: Store<fromRoot.State>) {}

  ngOnInit() {
    this.showSidebar$ = this.store.pipe(select(fromRoot.selectShowSidebar));
  }

  backdropClicked(){
    this.store.dispatch(toggleSidebar());
  }

}
