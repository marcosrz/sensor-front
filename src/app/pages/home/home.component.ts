import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Reading } from 'src/app/models/Reading';
import { loadReadings, setFrom, setTo } from 'src/app/store/reading/reading.actions';
import * as fromReading from '../../store/reading/reading.reducer' 
import * as fromRoot from '../../store' 
import { tap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public from$: Observable<Date>;
  public to$: Observable<Date>;

  public min$: Observable<Reading>;
  public max$: Observable<Reading>;
  public avg$: Observable<Reading>;
  public cur$: Observable<Reading>;
  public readings$: Observable<Reading[]>;

  public from: Date = new Date();
  public to: Date = new Date();

  fromInput = new FormControl(this.from);
  toInput = new FormControl(this.to);
  
  constructor(private store: Store<fromRoot.State>) {
    this.readings$ = this.store.pipe(select(fromRoot.selectReadingsState));
    this.min$ = this.store.pipe(select(fromRoot.selectMin));
    this.max$ = this.store.pipe(select(fromRoot.selectMax));
    this.avg$ = this.store.pipe(select(fromRoot.selectAvg));
    this.cur$ = this.store.pipe(select(fromRoot.selectCur));
    this.store.pipe(select(fromRoot.selectFrom)).subscribe(d => this.from = new Date(new Date(d).setHours(0,0,0,0)));
    this.store.pipe(select(fromRoot.selectTo)).subscribe(d => this.to = new Date(new Date(d).setHours(23,59,59,999)));
  }

  ngOnInit() {
    this.store.dispatch(setFrom({ data: new Date().toUTCString() }));
    this.store.dispatch(setTo({ data: new Date().toUTCString() }));
    this.store.dispatch(loadReadings());
  }

  search(){
    this.store.dispatch(loadReadings());
  }

  fromChange($event){
    this.store.dispatch(setFrom({ data: new Date($event.target.value).toUTCString()}));
  }
  
  toChange($event){
    this.store.dispatch(setTo({ data: new Date($event.target.value).toUTCString()}));
  }
}
