import { Component, OnInit, Input } from '@angular/core';
import { Reading } from 'src/app/models/Reading';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-current-reading',
  templateUrl: './current-reading.component.html',
  styleUrls: ['./current-reading.component.scss']
})
export class CurrentReadingComponent implements OnInit {

  @Input() title: String;
  @Input() description: String;
  @Input() reading$: Observable<Reading>;
  public value: Number;
  public unit: String;
  public parsedDate: Date = new Date();
  constructor() { }

  ngOnInit() {

    this.reading$.subscribe(reading => {
      this.value = reading.value;
      this.unit = reading.unit;
      this.parsedDate = new Date(reading.date);
    })

  }

}
