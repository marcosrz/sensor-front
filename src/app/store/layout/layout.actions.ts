import { createAction, props } from '@ngrx/store';

export const toggleSidebar = createAction(
  '[Layout] Toggle Sidebar'
);

export const showSidebar = createAction(
  '[Layout] Show Sidebar'
);

export const hideSidebar = createAction(
  '[Layout] Hide Sidebar'
);




