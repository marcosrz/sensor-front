import { createFeatureSelector, createSelector, ActionReducerMap, Action } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import * as fromReading from './reading/reading.reducer';
import * as fromLayout from './layout/layout.reducer';

export interface State {
  router: RouterReducerState
  [fromReading.readingFeatureKey]: fromReading.State,
  [fromLayout.layoutFeatureKey]: fromLayout.State,
}

export const selectReadingState = createFeatureSelector<State, fromReading.State>(fromReading.readingFeatureKey);
export const selectReadingsState = createSelector(selectReadingState, fromReading.selectReadings);
export const selectAvg = createSelector(selectReadingState, fromReading.selectAvg);
export const selectMax = createSelector(selectReadingState, fromReading.selectMax);
export const selectMin = createSelector(selectReadingState, fromReading.selectMin);
export const selectCur = createSelector(selectReadingState, fromReading.selectCur);
export const selectFrom = createSelector(selectReadingState, fromReading.selectFrom);
export const selectTo = createSelector(selectReadingState, fromReading.selectTo);

export const selectLayoutState = createFeatureSelector<State, fromLayout.State>(fromLayout.layoutFeatureKey);
export const selectShowSidebar = createSelector(selectLayoutState, fromLayout.selectShowSidebar);

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  reading: fromReading.reducer,
  layout: fromLayout.reducer,
};