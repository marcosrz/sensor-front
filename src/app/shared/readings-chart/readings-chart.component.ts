import { Component, OnInit, Input } from '@angular/core';
import { Reading } from 'src/app/models/Reading';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-readings-chart',
  templateUrl: './readings-chart.component.html',
  styleUrls: ['./readings-chart.component.scss']
})
export class ReadingsChartComponent implements OnInit {

  @Input() readings: Observable<Reading[]>;

  public labels: String[] = [];
  public type: String = 'line';
  public legend: Boolean = false;
  public data: any[] = null;
  public options = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false
        }
      }]
    }
  }

  constructor() {}

  ngOnInit() {

    this.readings.subscribe(data => {

      const dates = data.map(reading => new Date(reading.date));
      const labels = dates.map(date => `${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}`);

      this.labels = labels
      this.data = [
        { 
          data: data.map(reading => reading.value),
          label: 'Series A',
          backgroundColor: '#3f51b5AA',
          borderColor: '#3f51b5',
          pointBackgroundColor: '#3f51b5',
        }
      ];
    })

  }

}
