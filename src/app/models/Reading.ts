export class Reading {

  public _id: String;
  public value: Number;
  public raw: String;
  public unit: String;
  public sensor: String;
  public date: Date;

}