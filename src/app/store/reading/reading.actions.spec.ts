import * as fromReading from './reading.actions';

describe('loadReadings', () => {
  it('should return an action', () => {
    expect(fromReading.loadReadings().type).toBe('[Reading] Load Readings');
  });
});
