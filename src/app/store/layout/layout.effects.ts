import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { concatMap, switchMap } from 'rxjs/operators';
import { EMPTY, never } from 'rxjs';

import * as LayoutActions from './layout.actions';


@Injectable()
export class LayoutEffects {

  // loadLayouts$ = createEffect(() => {
  //   return this.actions$.pipe( 

  //     ofType(LayoutActions.loadLayouts),
  //     /** An EMPTY observable only emits completion. Replace with your own observable API request */
  //     switchMap(() => EMPTY)
      
  //   );
  // });

  constructor(private actions$: Actions) {}

}
