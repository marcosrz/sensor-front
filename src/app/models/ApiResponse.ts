export class ApiResponse<T> {

  public success: Boolean;
  public payload: T;

}