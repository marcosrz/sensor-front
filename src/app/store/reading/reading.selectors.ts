import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReading from './reading.reducer';

export const selectReadingState = createFeatureSelector<fromReading.State>(
  fromReading.readingFeatureKey
);
